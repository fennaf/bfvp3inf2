#!/usr/bin/env python3

"""
Description of the program/module
"""

__author__ = "...."

## CODE

# Imports
import sys


# Constants



# Functions





# Main
def main(args):

    # Preparation




    # Work




    # Finalize




    return 0

if __name__ == "__main__":
    exitcode = main(sys.argv)
    sys.exit(exitcode)
