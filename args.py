#!/usr/bin/env python3

"""
This program takes the arguments and prints them in reverse order
"""

__author__ = 'Fenna Feenstra'
__version__ = '0.1'

import sys

def print_everything(*args):
    for index, value in enumerate(args):
       print(index + 1, value)
    print('\n')

def main(args):
    args.reverse()
    print_everything(*args)
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
